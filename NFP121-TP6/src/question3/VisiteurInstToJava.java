package question3;

import java.io.*;

import question1.*;
import question2.*;

/**
 * 
 */
@SuppressWarnings("unused")
public class VisiteurInstToJava extends VisiteurInstruction<String> {

	private final static int TAB = 2;
	private static final String lineSeparator = System.getProperties().getProperty("line.separator");

	private VisiteurExpression<String> vi;
	private VisiteurExpressionBooleenne<String> vb;

	private int tabulations;

	/**
	 * Cr�ation d'un visiteur d'instructions
	 * 
	 * @param vi
	 *            le visiteur d'expressions arithm�tiques
	 * @param vb
	 *            le visiteur d'expression bool�ennes
	 * @param tabulations
	 *            tabulations initiales
	 */
	public VisiteurInstToJava(VisiteurExpression<String> _vi, VisiteurExpressionBooleenne<String> _vb, int _tabulations) {
		vi = _vi;
		vb = _vb;
		tabulations = _tabulations;
	}

	/**
	 * Cr�ation d'un visiteur d'instructions
	 * 
	 * @param vi
	 *            le visiteur d'expressions arithm�tiques
	 * @param vb
	 *            le visiteur d'expression bool�ennes
	 */
	public VisiteurInstToJava(VisiteurExpression<String> vi, VisiteurExpressionBooleenne<String> vb) {
		this(vi, vb, 0);
	}

	/**
	 * obtention du contexte, ici celui du visiteur d'expression arithm�tiques
	 * 
	 * @return le contexte ici de vi(le visiteur d'expression)
	 */
	public Contexte contexte() {
		return vi.contexte();
	}

	/**
	 * Visite d'une instance de la classe Affectation.
	 * 
	 * 
	 * @param a
	 *            une affectation
	 * @return a := exp
	 */
	public String visite(Affectation a) {
		return a.v().accepter(vi) + " = " + a.exp().accepter(vi);
	}

	/**
	 * Visiste d'une s�quence seq(I1,I2) <br>
	 * 
	 * @param seq
	 *            une s�quence
	 * @return i1;i2
	 */
	public String visite(Sequence seq) {
		return seq.i1().accepter(this) + ";" + lineSeparator + 
				seq.i2().accepter(this) + ";" + lineSeparator;
	}

	public String visite(Selection sel) {
		String str;
		str = "if (" + sel.cond().accepter(vb) + ") {" + lineSeparator + 
				sel.i1().accepter(this) + lineSeparator +
				"}";
		if (sel.i2() != null) {
			str += " else {" + lineSeparator + 
					sel.i2().accepter(this) + lineSeparator + 
					"}";
		}
		return str;
	}

	public String visite(TantQue tq) {
		String str;
		str = "while (" + tq.cond().accepter(vb) + ") {" + lineSeparator 
				+ tq.i1().accepter(this) + ";" + lineSeparator
				+ "}";
		return str;
	}

	public String visite(Pour pour) {
		String str;
		str = "for (" + pour.init().accepter(this) + "; " + pour.cond().accepter(vb) + "; " + pour.inc().accepter(this) + ") {" + lineSeparator 
				+ pour.i1().accepter(this) + ";" + lineSeparator
				+ "}";
		return str;
	}
	

	public String visite(Afficher a) {
		return "System.out.println(" + a.exp().accepter(vi) + ")";
	}

	public String visite(Assertion a) {
		return "assert " + a.cond().accepter(vb);
	}

	private String tab(int n) {
		String str = new String();

		str = str + lineSeparator;
		for (int i = 0; i < this.tabulations + n; i++) {
			str = str + " ";
		}
		this.tabulations += n;
		return str;
	}

}
