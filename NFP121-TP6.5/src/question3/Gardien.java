package question3;
import java.util.*;
public class Gardien{
    private Stack<Memento> mementoStk;
    
    public Gardien(){
      this.mementoStk = new Stack<Memento>();
    }
    public Memento getMemento() {
        return mementoStk.pop();
    }
    public void setMemento(Memento memento){
        mementoStk.push(memento);
    }
  }