package question1;

import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

/** Gestion par introspection des méthodes de la classe java.lang.Math,<br>
 *  Seules sont conservées :les méthodes retournant un double et d'arité 1 ou 2<p>
 *   Note : Emploi du Pattern Singleton pour cette table, accessible uniquement en lecture par des accesseurs<p>
 * La convention de nommage est la suivante :
 *        le "Nom" de la fonction suivi de "(double)"  exemple : "sqrt(double)"
 *  ou le "Nom" de la fonction suivi de "(double, double)"  exemple : "IEEEremainder(double, double)"
 */

@SuppressWarnings("unused")
final public class TableMethodesJavaLangMath{
  /** Singleton */
  private static TableMethodesJavaLangMath instanceUnique = null;

  public static TableMethodesJavaLangMath getInstance(){
    synchronized(TableMethodesJavaLangMath.class){
      if (instanceUnique==null){
        instanceUnique = new TableMethodesJavaLangMath();
      }
      return instanceUnique;
    }
  }
       
  private TableMethodesJavaLangMath(){
  }
  // fin du Singleton
 
  /**
   * @param  nomDeLaMethode Nom de la fonction + "(double)" ou "(double, double)"
   * @return true si la fonction est présente
   */
  public boolean cetteMethodeEstPresente(String nomDeLaMethode){
    return (tableDesMethodes.containsKey(nomDeLaMethode)); // à compléter
  }
 
  /**
   * @param  nomDeLaMethode Nom de la fonction + "(double)" ou "(double, double)"
   * @return true si la fonction est binaire, d'arité 2
   * @throws NoSuchElementException si la méthode demandée n'existe pas
   */
  public boolean cetteMethodeAttendDeuxParametres(String nomDeLaMethode){
     if(tableDesMethodes.containsKey(nomDeLaMethode)){
        // à compléter
        @SuppressWarnings("rawtypes")
		Class[] params = tableDesMethodes.get(nomDeLaMethode).getParameterTypes();
          return params.length==2;
     }
     else throw new java.util.NoSuchElementException();//La methode passé en parametre est absente de la Map
  }
 
  /**
   * @param  nomDeLaMethode Nom de la fonction + "(double)" ou "(double, double)"
   * @return true si la fonction est unaire, d'arité 1
   * @throws NoSuchElementException si la méthode demandée n'existe pas
   */
  public boolean cetteMethodeAttendUnSeulParametre(String nomDeLaMethode){
     if(tableDesMethodes.containsKey(nomDeLaMethode)){
        // à compléter
        @SuppressWarnings("rawtypes")
		Class[] params = tableDesMethodes.get(nomDeLaMethode).getParameterTypes();
          return params.length==1;
     }
     else throw new java.util.NoSuchElementException();//La methode passé en parametre est absente de la Map
  }
 
  /**
   * Obtention de la liste ordonnée des méthodes
   * @return la liste triée des fonctions issues de java.lang.Math
   */
  public String[] listeDesMethodes(){   
     String[] noms = tableDesMethodes.keySet().toArray(new String[tableDesMethodes.size()]);
     return noms;
  }
 
 /** Invocation d'une méthode de la table
   * @param  nomDeLaMethode Nom de la fonction + "(double)"
   * @param arg1 l'opérande
   * @return un résultat
   * @throws NoSuchElementException si la méthode demandée n'existe pas ou une exception levée par la fonction appelée
   */
  public double invoquer(String nomDeLaMethode,double arg1) throws Exception{
       Method m = tableDesMethodes.get(nomDeLaMethode);
       if(m==null) throw new NoSuchElementException();
       double resultat = (Double)m.invoke(null, new Object[] { new Double(arg1)});
       return resultat;
     }
 
 /** Invocation d'une méthode de la table
   * @param  nomDeLaMethode Nom de la fonction + "(double, double)"
   * @param arg1 l'opérande
   * @return un résultat
   * @throws NoSuchElementException si la méthode demandée n'existe pas ou une exception levée par la fonction appelée
   */ 
  public double invoquer(String nomDeLaMethode, double arg1, double arg2) throws Exception{
   // à compléter
       Method m = tableDesMethodes.get(nomDeLaMethode);
       if(m==null) throw new NoSuchElementException();
       double resultat = (Double)m.invoke(null, new Object[] { new Double(arg1), new Double(arg2)});
       return resultat;
     }
  /**
   * Le dictionnaire contient la liste des méthodes disponibles.
   * un choix de dictionnaire pourrait être pour la Clé une String soit le Nom de la fonction + "(double)" ou "(double, double)".<br>
   *  et en Valeur =  la Method correspondante.
   *  ou tout autre choix
   */
  private static Map<String,Method> tableDesMethodes = new TreeMap<String,Method>();// à compléter ...

  /** bloc statique d'intialisation de la table des méthodes */
  //*
   static{
    // à compléter
    
     for(Method m:Math.class.getMethods()){
        if(double.class==m.getGenericReturnType()){
           @SuppressWarnings("rawtypes")
		Class[] paramTypes = m.getParameterTypes();   
              if(paramTypes.length == 1) {           
                  if(double.class==paramTypes[0]) {     
                      tableDesMethodes.put(m.getName()+"(double)", m);     
                  }
              }
              if(paramTypes.length == 2) {       
                  if(double.class==paramTypes[0]&&double.class==paramTypes[1]){
                     tableDesMethodes.put(m.getName()+ "(double, double)", m);
                  }
              }
        }
     }   
  }
} 