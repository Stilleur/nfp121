package question3;
import question1.*;
import java.util.*;

public class TransactionDebitSansVisiteur extends AbstractTransaction {
  LinkedList<Integer> soldes = new LinkedList<Integer>();
  
  public TransactionDebitSansVisiteur(Cotisant compte){
		super(compte);
		soldes = new LinkedList<Integer>();
	}
	
  public void beginTransaction(){
    if(cotisant instanceof GroupeDeContributeurs){
      GroupeDeContributeurs g = (GroupeDeContributeurs)cotisant;
      Iterator<Cotisant> it = g.iterator();
      while(it.hasNext()){
        Cotisant cpt = it.next();
        if(cpt instanceof Contributeur)soldes.add(cpt.solde());
      }
    }else{
      soldes.add(cotisant.solde());
    }
//      System.out.print("beginTransaction: ");
//      System.out.println(soldes);
  }
  
  public void endTransaction(){
    soldes.clear();
  }
  
  public  void rollbackTransaction(){
    if(cotisant instanceof GroupeDeContributeurs){
      GroupeDeContributeurs g = (GroupeDeContributeurs)cotisant;
      Iterator<Cotisant> it = g.iterator();
      while(it.hasNext()){
        Cotisant cpt = it.next();
        if(cpt instanceof Contributeur)
          ((Contributeur)cpt).affecterSolde(soldes.removeFirst());
      }

    }else{
      ((Contributeur)cotisant).affecterSolde(soldes.removeFirst());
    }
//     System.out.print("rollbackTransaction: ");
//     System.out.println(soldes);
    soldes.clear();
  }

}