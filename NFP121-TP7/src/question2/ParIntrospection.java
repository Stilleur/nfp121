package question2;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.Iterator;
import java.util.NoSuchElementException;

@SuppressWarnings("unused")
public class ParIntrospection {
	public static void lierObservableEtObserver(Object observable, Object observer) throws Exception {
		if (observable == null) {
			throw new NoSuchElementException("obj model (observable) NULL");
		}
		if (observer == null) {
			throw new NoSuchElementException("listener NULL");
		}
		Class source_ext = observable.getClass();
		Class[] observer_int = observer.getClass().getInterfaces();
		if (!observer_int[0].getName().endsWith("Observer")) throw new NoSuchElementException();
		Method[] source_methodes = source_ext.getMethods();
		boolean isObservable = false;
		for (int i = 0; i < source_methodes.length; i++) {
			if (source_methodes[i].getName().startsWith("addObserver")) {
				try {
					source_methodes[i].invoke(observable, observer);
					isObservable = true;
				} catch (Exception e) {}
			}
		}
		if (!isObservable) throw new NoSuchElementException();
	}
	public static void delierObservableEtObserver(Object observable, Object observer) throws Exception {
		boolean actif = true;
		if (observable == null) {
			throw new NoSuchElementException("obj model (observable) NULL");
		}
		if (observer == null) {
			throw new NoSuchElementException("listener NULL");
		}
		if (!(observable instanceof java.util.Observable)) {
			throw new NoSuchElementException("le model n'est pas Observable");
		}
		if (!(observer instanceof java.util.Observer)) {
			throw new NoSuchElementException("le listener n'est pas Observer");
		}
		Class <? > modelClass = observable.getClass();
		for (Method method: modelClass.getMethods()) {
			try {
				if (method.getName() == "deleteObserver") {
					method.invoke(observable, observer);
					actif = false;
				}
			} catch (Exception e) {
				throw new NoSuchElementException("Une erreur a été levé");
			}
			if (actif) {
				throw new NoSuchElementException("aucune méthode n'a été invoquée");
			}
		}
	}
	public static void lierSourceEtListener(Object source, Object listener) throws Exception {
		boolean actif = true;
		if (source == null) throw new NoSuchElementException();
		if (listener == null) throw new NoSuchElementException();
		Class <? > sourceClass = source.getClass();
		for (Method method: sourceClass.getMethods()) {
			if (method.getParameterTypes().length == 1) {
				Class c = method.getParameterTypes()[0];
				String n = method.getName().toString();
				if (c.isInstance(listener) && n.contains("add")) {
					try {
						method.invoke(source, listener);
						actif = false;
					} catch (Exception e) {
						throw new NoSuchElementException("Une erreur a été levé");
					}
				}
			}
			if (actif) {
				throw new NoSuchElementException("Aucune méthode n'a été invoquée");
			}
		}
	}
	public static void delierSourceEtListener(Object source, Object listener) throws Exception {
		boolean actif = true;
		if (source == null) {
			throw new NoSuchElementException("la source n'a pas été fournie ");
		}
		if (listener == null) {
			throw new NoSuchElementException("le listener n'a pas été fournie ");
		}
		Class <? > sourceClass = source.getClass();
		for (Method m: sourceClass.getMethods()) {
			if (m.getParameterTypes().length == 1) {
				Class c = m.getParameterTypes()[0];
				String n = m.getName().toString();
				if (c.isInstance(listener) && n.contains("remove")) {
					try {
						m.invoke(source, listener);
						actif = false;
					} catch (Exception e) {
						throw new NoSuchElementException("Une erreur a été levé");
					}
				}
			}
		}
		if (actif) {
			throw new NoSuchElementException("Aucune méthode n'a été invoquée");
		}
	}
}