package question2;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

@SuppressWarnings("serial")
public class IHMListe2 extends JPanel implements ActionListener, ItemListener {

    private JPanel cmd = new JPanel();
    private JLabel afficheur = new JLabel();
    private JTextField saisie = new JTextField();

    private JPanel panelBoutons = new JPanel();
    private JButton boutonRechercher = new JButton("rechercher");
    private JButton boutonRetirer = new JButton("retirer");

    private CheckboxGroup mode = new CheckboxGroup();
    private Checkbox ordreCroissant = new Checkbox("croissant", mode, false);
    private Checkbox ordreDecroissant = new Checkbox("d�croissant", mode, false);

    private JButton boutonOccurrences = new JButton("occurrence");

    private JButton boutonAnnuler = new JButton("annuler");

    private TextArea texte = new TextArea();

    private List<String> liste;
    private Map<String, Integer> occurrences;
    
    private Stack<IHMListe2Memento> mementos;

    public IHMListe2(List<String> liste, Map<String, Integer> occurrences) {
        this.liste = liste;
        this.occurrences = occurrences;

        cmd.setLayout(new GridLayout(3, 1));

        cmd.add(afficheur);
        cmd.add(saisie);

        panelBoutons.setLayout(new FlowLayout(FlowLayout.LEFT));
        panelBoutons.add(boutonRechercher);
        panelBoutons.add(boutonRetirer);
        panelBoutons.add(new JLabel("tri du texte :"));
        panelBoutons.add(ordreCroissant);
        panelBoutons.add(ordreDecroissant);
        panelBoutons.add(boutonOccurrences);
        panelBoutons.add(boutonAnnuler);
        cmd.add(panelBoutons);

        afficheur.setText(liste.getClass().getName() + " et "
            + occurrences.getClass().getName());
        texte.setText(liste.toString());

        setLayout(new BorderLayout());

        add(cmd, "North");
        add(texte, "Center");

        boutonRechercher.addActionListener(this);
        boutonRetirer.addActionListener(this);
        boutonOccurrences.addActionListener(this);
        ordreCroissant.addItemListener(this);
        ordreDecroissant.addItemListener(this);
        boutonAnnuler.addActionListener(this);
        // � compl�ter;
        
        mementos = new Stack<IHMListe2Memento>();
    }

    public void actionPerformed(ActionEvent ae) {
        try {
            boolean res = false;
            if (ae.getSource() == boutonRechercher || ae.getSource() == saisie) {
                res = liste.contains(saisie.getText());
                afficheur.setText("r�sultat de la recherche de : "
                    + saisie.getText() + " -->  " + res);
            } else if (ae.getSource() == boutonRetirer) {
                res = retirerDeLaListeTousLesElementsCommencantPar(saisie
                    .getText());
                afficheur
                .setText("r�sultat du retrait de tous les �l�ments commen�ant par -->  "
                    + saisie.getText() + " : " + res);
            } else if (ae.getSource() == boutonOccurrences) {
                Integer occur = occurrences.get(saisie.getText());
                if (occur != null)
                    afficheur.setText(" -->  " + occur + " occurrence(s)");
                else
                    afficheur.setText(" -->  ??? ");
            } else if (ae.getSource() == boutonAnnuler) {
            	res = annuler();
            }
            texte.setText(liste.toString());

        } catch (Exception e) {
            afficheur.setText(e.toString());
        }
    }
    
    private boolean annuler() {
    	IHMListe2Memento memento;
    	
    	if ((memento = mementos.pop()) != null) {
    		liste = memento.getListe();
    		occurrences = memento.getOccurrences();
    		return true;
    	}
    	
    	return false;
    }

    public void itemStateChanged(ItemEvent ie) {
        if (ie.getSource() == ordreCroissant) {
        	mementos.push(new IHMListe2Memento(new LinkedList<String>(liste), new HashMap<String, Integer>(occurrences)));
        	Collections.sort(liste);// � compl�ter
        }
        else if (ie.getSource() == ordreDecroissant) {
        	mementos.push(new IHMListe2Memento(new LinkedList<String>(liste), new HashMap<String, Integer>(occurrences)));
        	Collections.sort(liste, Collections.reverseOrder());;// � compl�ter
        }

        texte.setText(liste.toString());
    }

    private boolean retirerDeLaListeTousLesElementsCommencantPar(String prefixe) {
    	boolean r = false;
    	LinkedList<String> mListe = new LinkedList<String>(liste);
    	HashMap<String, Integer> mOcc = new HashMap<String, Integer>(occurrences);
    	
    	for (Iterator<String> str = liste.iterator(); str.hasNext();) {
			String string = (String) str.next();
			if (string.startsWith(prefixe)) {
				occurrences.put(string, 0);
				str.remove();
				r = true;
			}
		}
    	
    	if (r) {
    		mementos.add(new IHMListe2Memento(mListe, mOcc));
    	}

    	return r;
    }

}