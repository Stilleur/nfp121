package question3;

import question3.tp3.PileI;
import question3.tp3.PilePleineException;
import question3.tp3.PileVideException;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

/**
 * D�crivez votre classe Controleur ici.
 * 
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
@SuppressWarnings({ "serial", "unused" })
public class Controleur extends JPanel implements ActionListener{

	private JButton push, add, sub, mul, div, clear;
	private PileModele<Integer> pile;
	private JTextField donnee;

	public Controleur(PileModele<Integer> pile) {
		super();
		this.pile = pile;
		this.donnee = new JTextField(8);

		this.push = new JButton("PUSH");
		this.add = new JButton("+");
		this.sub = new JButton("-");
		this.mul = new JButton("*");
		this.div = new JButton("/");
		this.clear = new JButton("CLEAR");

		setLayout(new GridLayout(2, 1));
		add(donnee);
		donnee.addActionListener(this);
		JPanel boutons = new JPanel();
		boutons.setLayout(new FlowLayout());
		boutons.add(push);  push.addActionListener(this);
		boutons.add(add);   add.addActionListener(this);
		boutons.add(sub);   sub.addActionListener(this);
		boutons.add(mul);   mul.addActionListener(this);
		boutons.add(div);   div.addActionListener(this);
		boutons.add(clear); clear.addActionListener(this);
		add(boutons);
		boutons.setBackground(Color.red);
		actualiserInterface();
	}

	public void actualiserInterface() {
		repaint();
	}

	private Integer operande() throws NumberFormatException {
		return Integer.parseInt(donnee.getText());
	}

	public void actionPerformed(ActionEvent ae) {
		int o1, o2, r;
		switch (ae.getActionCommand()) {

		case "PUSH":
			try {
				pile.empiler(operande());
			} catch (NumberFormatException e) {
				donnee.setText("ERR : Entier demand�");
			} catch (PilePleineException e) {
				donnee.setText("ERR : Pile pleine");
			}
			donnee.setText("");
			break;

		case "CLEAR":
			while (!pile.estVide()) {
				try {
					pile.depiler();
				} catch (PileVideException e) {
					// IMPOSSIBLE
				}
			}
			break;

		case "+":
			Integer nb1 = null;
			Integer nb2 = null;

			try {
				nb1 = pile.depiler();
				nb2=pile.depiler();
				pile.empiler(nb1 +nb2);

			} catch (PileVideException | PilePleineException e) {
				try {
					if(nb2 != null)
						pile.empiler(nb2);
					if(nb1 != null)
						pile.empiler(nb1);
				} catch (PilePleineException e2) {

				}
			}
			
			break;
			
			
//			if(pile.taille() > 1) {
//				try {
//					o1 = pile.depiler();
//					o2 = pile.depiler();
//					r = o2 + o1;
//					pile.empiler(r);
//				} catch (PileVideException e) {
//					// IMPOSSIBLE
//				} catch (PilePleineException e) {
//					// IMPOSSIBLE
//				}
//			}
//			else {
//				//throw new IllegalArgumentException("test+");
//				//donnee.setText("ERR : Il faut au moins deux op�randes");
//			}
//			break;

		case "-":
			if(pile.taille() > 1) {
				try {
					o1 = pile.depiler();
					o2 = pile.depiler();
					r = o2 - o1;
					pile.empiler(r);
				} catch (PileVideException e) {
					// IMPOSSIBLE
				} catch (PilePleineException e) {
					// IMPOSSIBLE
				}
			}
			else {
				donnee.setText("ERR : Il faut au moins deux op�randes");
			}
			break;

		case "*":
			if(pile.taille() > 1) {
				try {
					o1 = pile.depiler();
					o2 = pile.depiler();
					r = o2 * o1;
					pile.empiler(r);
				} catch (PileVideException e) {
					// IMPOSSIBLE
				} catch (PilePleineException e) {
					// IMPOSSIBLE
				}
			}
			else {
				donnee.setText("ERR : Il faut au moins deux op�randes");
			}
			break;

		case "/":
			if(pile.taille() > 1) {
				try {
					o1 = pile.depiler();
					if(o1 == 0) {
						pile.empiler(o1);
						break;
					}
					o2 = pile.depiler();
					r = o2 / o1;
					pile.empiler(r);
				} catch (PileVideException e) {
					// IMPOSSIBLE
				} catch (PilePleineException e) {
					// IMPOSSIBLE
				}
			}
			else {
				donnee.setText("ERR : Il faut au moins deux op�randes");
			}
			break;
		}
		
		actualiserInterface();
	}

	// � compl�ter
	// en cas d'exception comme division par z�ro, mauvais format de nombre
	// la pile reste en l'�tat (intacte)
}
