package question1;

import java.util.*;

public class Ensemble<T> extends AbstractSet<T> {

	protected java.util.Vector<T> table = new java.util.Vector<T>();

	public int size() {
		return table.size();
	}

	public Iterator<T> iterator() {
		return table.iterator();
	}

	public boolean add(T t) {
		// à compléter pour la question1-1
		
		// Si l'�l�ment existe dans l'ensemble, on ne fait rien
		if (table.contains(t)) {
			return false;
		}
		
		// Sinon on le rajoute
		return table.add(t);
	}

	public Ensemble<T> union(Ensemble<? extends T> e) {
		// à compléter pour la question1-2
		Ensemble<T> ensemble = new Ensemble<T>();
		ensemble.addAll(this);
		ensemble.addAll(e);
		return ensemble;
	}

	public Ensemble<T> inter(Ensemble<? extends T> e) {
		// à compléter pour la question1-2
		Ensemble<T> ensemble = new Ensemble<T>();
		ensemble.addAll(this);
		ensemble.retainAll(e);
		return ensemble;
	}

	public Ensemble<T> diff(Ensemble<? extends T> e) {
		// à compléter pour la question1-2
		Ensemble<T> ensemble = new Ensemble<T>();
		ensemble.addAll(this);
		ensemble.removeAll(e);
		return ensemble;
	}

	Ensemble<T> diffSym(Ensemble<? extends T> e) {
		// à compléter pour la question1-2
		Ensemble<T> ensTamp = new Ensemble<T>();
	      ensTamp.addAll(this);
	      ensTamp.addAll( e );
	      Ensemble<T> ensTampInter = new Ensemble<T>();
	      ensTampInter=this.inter( e );
	      ensTamp.removeAll(ensTampInter);
	      return ensTamp;
	}
	
}
