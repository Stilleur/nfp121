package question2;

import javax.swing.*;

import java.util.*;

/**
 * Classe AppletteListe2 - d�crivez la classe ici
 * 
 * @author: (votre nom)
 * @version: (un num�ro de version ou une date)
 */
@SuppressWarnings("serial")
public class AppletteListe2 extends JApplet {

    public void init() {
        // Il y a un conflit de s�curit� avec les navigateurs courants (incluant
        // Netscape & Internet Explorer) qui interdisent l'acc�s � la queue
        // d'�v�nements d'AWT --ce dont les JApplets ont besoin au d�marrage.
        JRootPane rootPane = this.getRootPane();
        rootPane.putClientProperty("defeatSystemEventQueueCheck", Boolean.TRUE);

        List<String> liste = Chapitre2CoreJava2.listeDesMots();
        Map<String, Integer> table = Chapitre2CoreJava2.occurrencesDesMots(liste);
        IHMListe2 ihmListe = new IHMListe2(liste, table);
        add(ihmListe);
    }

}
