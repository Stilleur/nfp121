package question3;

//import com.sun.org.apache.xml.internal.serialize.LineSeparator;

import question1.*;
import question2.*;

/**
 * Visisteur d'instruction, chaque classe concrète possède une implémentation de
 * la visite
 * 
 */
public class VisiteurInstToString extends VisiteurInstruction<String> {

	private VisiteurExpression<String> vi;
	private VisiteurExpressionBooleenne<String> vb;

	/**
	 * Création d'un visiteur d'instructions
	 * 
	 * @param vi
	 *            le visiteur d'expressions arithmétiques
	 * @param vb
	 *            le visiteur d'expression booléennes
	 */
	public VisiteurInstToString(VisiteurExpression<String> _vi,
			VisiteurExpressionBooleenne<String> _vb) {
		vi = _vi;
		vb = _vb;
	}

	/**
	 * obtention du contexte, ici celui du visiteur d'expression arithmétiques
	 * 
	 * @return le contexte ici de vi(le visiteur d'expression)
	 */
	public Contexte contexte() {
		return vi.contexte();
	}

	/**
	 * Visite d'une instance de la classe Affectation.
	 * 
	 * 
	 * @param a
	 *            une affectation
	 * @return a := exp
	 */
	public String visite(Affectation a) {
//		String str;
//		str = a.v().accepter(vi) + " := " + a.exp().accepter(vi);
//		return str;
		return a.v().accepter(this.vi) + " := " + a.exp().accepter(this.vi);
	}

	/**
	 * Visiste d'une séquence seq(I1,I2) <br>
	 * 
	 * @param seq
	 *            une séquence
	 * @return i1;i2
	 */
	public String visite(Sequence seq) {
//		String str;
//		str = seq.i1().accepter(this) + " ; " + seq.i2().accepter(this);
//		return str;
		return seq.i1().accepter(this) + " ; " + seq.i2().accepter(this);
	}

	public String visite(Selection sel) {
//		String str;
//		str = "if (" + sel.cond().accepter(vb) + ") {" + LineSeparator.Windows 
//				+ sel.i1().accepter(this) + LineSeparator.Windows +
//				"}";
//		if (sel.i2() != null) {
//			str += " else {" + LineSeparator.Windows 
//					+ sel.i2().accepter(this) + LineSeparator.Windows 
//					+ "}";
//		}
//		return str;
		String str = "si" + sel.cond().accepter(this.vb) + " alors " + sel.i1().accepter(this);
		if (sel.i2() != null) str = str + " sinon " + sel.i2().accepter(this);

		return str + " fsi";
	}

	public String visite(TantQue tq) {
//		String str;
//		str = "while (" + tq.cond().accepter(vb) + ") {" + LineSeparator.Windows 
//				+ tq.i1().accepter(this) + ";" + LineSeparator.Windows 
//				+ "}";
//		return str;
		return "tantque" + tq.cond().accepter(this.vb) + " faire " + tq.i1().accepter(this) + " ftq";
	}

	public String visite(Pour pour) {
//		String str;
//		str = "for (" + pour.init().accepter(this) + "; " + pour.cond().accepter(vb) + "; "
//				+ pour.inc().accepter(this) + ") {" + LineSeparator.Windows 
//				+ pour.i1().accepter(this) + ";" + LineSeparator.Windows
//				+ "}";
//		return str;
		return "pour(" + pour.init().accepter(this) + "," + pour.cond().accepter(this.vb) + ","
		+ pour.inc().accepter(this) + ") " + pour.i1().accepter(this)
		+ " fpour";
	}

	public String visite(Afficher a) {
//		String str;
//		str = "System.out.println(" + a.exp().accepter(vi) + ")";
//		return str;
		return "afficher(" + a.exp().accepter(this.vi) + ")";
	}

	public String visite(Assertion a) {
//		String str;
//		str = "assert " + a.cond().accepter(vb);
//		return str;
		return "assertion(" + a.cond().accepter(this.vb) + ")";
	}

}
