package question3;

import question1.*;
import java.util.*;

public class Memento {
     private Map<Cotisant,Integer> cotisants ;
     
     //public Memento(Cotisant c) throws MementoException{
     public Memento(Cotisant c) {
       cotisants = new HashMap<Cotisant,Integer>();
       c.accepter( new VisiteurSauvegarde(cotisants) ) ;
     }

//     public void setState(Cotisant c) throws MementoException  {
     public void setState(Cotisant c) {
       c.accepter( new VisiteurRestitution(cotisants) ) ;
     }
    
      private static class VisiteurSauvegarde implements Visiteur<Void> {
        private Map<Cotisant,Integer> cotisants;
        
        public VisiteurSauvegarde(Map<Cotisant,Integer> cotisants){
          this.cotisants = cotisants;
        }
        
        public Void visite(Contributeur c){
            cotisants.put( c, c.solde() ) ;
            return null;
        }

        public Void visite(GroupeDeContributeurs g){
            for(Cotisant c : g.getChildren()) {
                c.accepter(this) ;
            }
            return null;
        }
      }
    
     private static class VisiteurRestitution implements Visiteur<Void> {
       private Map<Cotisant,Integer> cotisants;
        
        public VisiteurRestitution(Map<Cotisant,Integer> cotisants){
          this.cotisants = cotisants;
        }
        public Void visite(Contributeur c){
            c.affecterSolde( cotisants.get( c ) ) ;
            return null ;
        }

        public Void visite(GroupeDeContributeurs g){
            for(Cotisant c : g.getChildren()) {
                c.accepter(this) ;
            }
            return null ;
        }
    }
   } 