package question3;

@SuppressWarnings("serial")
public class MementoException extends Exception{
  public MementoException(){
    super();
  }

  public MementoException(String message){
    super(message);
  }
}
