package question1;

import org.junit.Test;

public class EnsembleTest extends junit.framework.TestCase {
	// Définissez ici les variables d'instance nécessaires à vos engagements;
	// Vous pouvez également les saisir automatiquement du présentoir
	// à l'aide du menu contextuel "Présentoir --> Engagements".
	// Notez cependant que ce dernier ne peut saisir les objets primitifs
	// du présentoir (les objets sans constructeur, comme int, float, etc.).
	
	private Ensemble<Integer> ensembleInteger;
	private Ensemble<Integer> ensembleIntegerBis;
	private Ensemble<Integer> ensembleIntegerBisBis;
	private Ensemble<Ensemble<Integer>> ensembleEnsembleInteger;

	/**
	 * Met en place les engagements.
	 * 
	 * Méthode appelée avant chaque appel de méthode de test.
	 * @throws Exception 
	 */
	protected void setUp() throws Exception // throws java.lang.Exception
	{
		super.setUp();
		ensembleInteger = new Ensemble<Integer>();
		ensembleIntegerBis = new Ensemble<Integer>();
		ensembleIntegerBisBis = new Ensemble<Integer>();
		ensembleEnsembleInteger = new Ensemble<Ensemble<Integer>>();
	}

	/**
	 * Supprime les engagements
	 * 
	 * Méthode appelée aprés chaque appel de méthode de test.
	 */
	protected void tearDown() // throws java.lang.Exception
	{
		ensembleInteger = null;
		ensembleIntegerBis = null;
		ensembleIntegerBisBis = null;
		ensembleEnsembleInteger = null;
	}
	
	@Test
	public void testAdd() {
		assertTrue(ensembleInteger.add(1));
		assertTrue(ensembleInteger.add(2));
		assertFalse(ensembleInteger.add(2));
		assertTrue(ensembleInteger.add(3));
		assertFalse(ensembleInteger.add(1));
		assertFalse(ensembleInteger.add(3));
		
		ensembleIntegerBis.add(3);
		ensembleIntegerBis.add(4);
		ensembleIntegerBis.add(5);
		
		ensembleIntegerBisBis.add(1);
		ensembleIntegerBisBis.add(2);
		ensembleIntegerBisBis.add(3);
		
		assertTrue(ensembleEnsembleInteger.add(ensembleInteger));
		assertFalse(ensembleEnsembleInteger.add(ensembleInteger));
		assertTrue(ensembleEnsembleInteger.add(ensembleIntegerBis));
		assertFalse(ensembleEnsembleInteger.add(ensembleIntegerBisBis));
	}
	
	@Test
	public void testInter() {
		ensembleInteger.add(2);
		ensembleInteger.add(3);
		ensembleInteger.add(4);
		ensembleInteger.add(5);
		
		ensembleIntegerBis = ensembleInteger.inter(ensembleInteger);
		assertEquals(ensembleInteger, ensembleIntegerBis);
	}

	/**
	 * Il ne vous reste plus qu'à définir une ou plusieurs méthodes de test. Ces
	 * méthodes doivent vérifier les résultats attendus à l'aide d'assertions
	 * assertTrue(<boolean>). Par convention, leurs noms devraient débuter par
	 * "test". Vous pouvez baucher le corps grâce au menu contextuel
	 * "Enregistrer une méthode de test".
	 */
	public void testUnion() {
		question1.Ensemble<Integer> e1, e2;
		e1 = new question1.Ensemble<Integer>();
		assertEquals(true, e1.add(2));
		assertEquals(true, e1.add(3));

		e2 = new question1.Ensemble<Integer>();
		assertEquals(true, e2.add(3));
		assertEquals(true, e2.add(4));

		question1.Ensemble<Integer> union = e1.union(e2);
		assertEquals(3, union.size());
		assertTrue(union.contains(2));
		assertTrue(union.contains(3));
		assertTrue(union.contains(4));
	}
	
}
