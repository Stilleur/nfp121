package question2;

import java.util.List;
import java.util.Map;

public class IHMListe2Memento {
	private List<String> liste;
	private Map<String, Integer> occurrences;
	
	public IHMListe2Memento(List<String> _liste, Map<String, Integer> _occurrences) {
		liste = _liste;
		occurrences = _occurrences;
	}

	public List<String> getListe() {
		return liste;
	}

	public Map<String, Integer> getOccurrences() {
		return occurrences;
	}
}
