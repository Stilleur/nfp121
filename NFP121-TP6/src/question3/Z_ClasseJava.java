package question3;

import java.util.Iterator;
import question1.Contexte;

import java.lang.reflect.*;
import java.io.*;

import java.util.*;

// http://stackoverflow.com/questions/10314904/no-com-sun-tools-javac-in-jdk7
// puis
// http://stackoverflow.com/questions/860187/access-restriction-on-class-due-to-restriction-on-required-library-rt-jar

public class Z_ClasseJava{
	private static final String lineSeparator = System.getProperties().getProperty("line.separator");

	private String nomDeLaClasse;
	private String nomDuPaquetage;
	private VisiteurInstruction<String> vi;
	private Instruction inst;

	public Z_ClasseJava(String nomDeLaClasse, Instruction inst, VisiteurInstruction<String> vi){
		this(nomDeLaClasse,null,inst,vi);
	}

	public Z_ClasseJava(String nomDeLaClasse, String nomDuPaquetage, Instruction inst, VisiteurInstruction<String> vi){
		this.nomDeLaClasse = nomDeLaClasse;
		this.nomDuPaquetage = nomDuPaquetage;
		this.vi   = vi;
		this.inst = inst;
	}

	public String enTete(){
		String str = new String();
		if(nomDuPaquetage !=null) str = str + "package " + nomDuPaquetage +";" + lineSeparator;
		str = str + lineSeparator + "/** NFP121/2006 question3, tp6" + lineSeparator + "   source Java genere par l'intermediaire de votre visiteur \"VisiteurInstToJava\"" + lineSeparator + "*/" + lineSeparator;
		return str;
	}

	public String declarations(){
		String str = lineSeparator;
		Contexte ctxt = vi.contexte();
		Iterator<String> it =ctxt.iterator();
		while(it.hasNext()){
			String identifiant = it.next();
			String valeur = Integer.toString(ctxt.lire(identifiant));
			str = str + "    " + "int " + identifiant + "=" + valeur + ";" + lineSeparator;
		}
		return str; 
	}

	public String instructions(){
		return inst.accepter(vi);
	}

	public String sourceComplet(){
		return sourceComplet(nomDeLaClasse);
	}

	private String sourceComplet(String className){
		StringBuffer sb = new StringBuffer();

		sb.append(enTete());
		sb.append("public class " + className + "{" + lineSeparator + lineSeparator);
		sb.append("  " + "public static void main(String[] args)throws Exception{");
		sb.append(declarations() + instructions());
		sb.append(lineSeparator + "  }" + lineSeparator);
		sb.append(enPied());

		return sb.toString();
	}

	public String enPied(){
		return lineSeparator + "}" + lineSeparator;
	}

	public void enFichier() throws IOException{
		new File("question3/" + nomDeLaClasse + " .java").delete();
		new File("question3/" + nomDeLaClasse + " .class").delete();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(new File("question3/" + nomDeLaClasse + ".java")));
			out.write(this.sourceComplet());
			out.close();
		} catch (IOException e) {
		}
	}

	/*  
	 * 
   The com.sun.tools.javac.Main class provides two static methods to invoke the compiler from a program:

   public static int compile(String[] args);
   public static int compile(String[] args, PrintWriter out);

   voir javatip 131, javaworld : http://www.javaworld.com/javatips/jw-javatip131.html
	 */


	private com.sun.tools.javac.Main javac = new com.sun.tools.javac.Main();

	@SuppressWarnings("static-access")
	public void compiler() throws Exception{
		File file=File.createTempFile(nomDeLaClasse,".java", new File(System.getProperty("user.dir")));
		file.deleteOnExit();

		String fileName=file.getName();
		String className=fileName.substring(0,fileName.length()-5);

		PrintWriter out = new PrintWriter(new FileOutputStream(file));
		out.print(sourceComplet(className));
		out.flush();
		out.close();

		String[] args = new String[] { "-d",System.getProperty("user.dir"),fileName};
		StringWriter sw = new StringWriter(); 
		int status = javac.compile(args,new PrintWriter(sw));
		new File(file.getParent(), className + ".class").deleteOnExit();
		if(status != 0) throw new Exception(showStatus(status));
	}

	@SuppressWarnings({ "static-access", "unchecked", "rawtypes" })
	public void compilerEtExecuter() throws Exception{
		File file=File.createTempFile(nomDeLaClasse,".java", new File(System.getProperty("user.dir")));
		file.deleteOnExit();

		String fileName=file.getName();
		String className=fileName.substring(0,fileName.length()-5);

		PrintWriter out = new PrintWriter(new FileOutputStream(file));
		out.print(sourceComplet(className));
		out.flush();
		out.close();
		//     System.out.println(className + "   " + sourceComplet(className));

		String[] args = new String[] { "-d",System.getProperty("user.dir"),fileName};
		StringWriter sw = new StringWriter(); 
		int status = javac.compile(args,new PrintWriter(sw));
		new File(file.getParent(), className + ".class").deleteOnExit();
		if(status != 0) throw new Exception(showStatus(status));

		if(status == 0){
			try {
				Class clazz = Class.forName(className,true,new Z_MyClassLoader());
				//Class clazz = Class.forName(className);
				Method main = clazz.getMethod("main", new Class[] { String[].class });
				main.invoke(null, new Object[] { new String[0] });
			}catch (InvocationTargetException ex) {
				ex.getTargetException().printStackTrace(new PrintWriter(sw));
				throw new Exception(" erreur !! " + sw.toString());
			}catch (Exception ex){
				ex.printStackTrace(new PrintWriter(sw));
				throw new Exception(" erreur !! " + sw.toString());
			}

		}
	}


	private String showStatus(int status){
		switch (status) {
		case 1: return "ERREUR de syntaxe, utilisez javac avant de soumettre ..."; 
		case 2: return "ERREUR de commande"; 
		case 3: return "ERREUR Systeme";
		case 4: return "Anormal ?";
		default:
			return "ERREUR au numero inconnu";
		}
	}

	private class Z_MyClassLoader extends ClassLoader{ 
		@SuppressWarnings("rawtypes")
		private Map<String,Class> classes = new HashMap<String,Class>();


		@SuppressWarnings({ "unchecked", "rawtypes" })
		protected synchronized Class loadClass(String name,boolean resolve) throws ClassNotFoundException{ 

			Class cl = (Class)classes.get(name);

			if (cl == null) {  
				// load class bytes--details depend on class loader

				byte[] classBytes = loadClassBytes(name);
				if (classBytes == null){
					//trace("findSystemClass : " + name);
					return findSystemClass(name);
				}

				cl = defineClass(name, classBytes, 0, classBytes.length);
				if (cl == null)
					throw new ClassNotFoundException(name);

				classes.put(name, cl); // remember class
			}

			//trace("resolve : " + resolve);
			//trace("classes : " + classes);

			if (resolve) resolveClass(cl);

			return cl;
		}

		private byte[] loadClassBytes(String name){  
			String cname = name.replace('.', '/') + ".class";
			FileInputStream in = null;
			try{  
				in = new FileInputStream(cname);
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1){
					byte b = (byte)(ch);
					buffer.write(b);
				}
				in.close();
				return buffer.toByteArray();
			}catch (IOException e){
				if (in != null){  try { in.close(); } catch (IOException e2) { }
				}
				return null;
			}
		}
	}
}