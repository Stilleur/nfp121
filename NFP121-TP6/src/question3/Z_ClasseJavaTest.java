package question3;

import question1.*;
import question2.*;

import java.lang.reflect.*;

@SuppressWarnings("unused")
public class Z_ClasseJavaTest extends junit.framework.TestCase{


	public void test_CompilationDeFactoriel(){
		Contexte m = new Memoire();
		Variable x = new Variable(m,"x",5);
		Variable fact = new Variable(m,"fact",1);

		Instruction inst = 
				new TantQue(
						new Sup(x,new Constante(1)),
						new Sequence(
								new Affectation(fact,new Multiplication(fact,x)),
								new Affectation(x,new Soustraction(x,new Constante(1))))
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Fact", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compiler();
		}catch(Exception e){
			//e.printStackTrace();
			fail("compilation de Sup, Affectation,Sequence,TantQue ??? " + e.getMessage());
		}
	}     


	public void test_CompilationDeSomme() throws Exception{
		Contexte m = new Memoire();
		Variable n = new Variable(m,"n",100);
		Variable s = new Variable(m,"s",0);
		Variable i = new Variable(m,"i",0);

		Instruction inst =
				new Sequence(
						new Sequence(
								new TantQue(
										new Inf(i,n),
										new Sequence(
												new Affectation(i,new Addition(i,new Constante(1))),
												new Affectation(s,new Addition(s,i)))
										),
										new Afficher(s)),
										new Assertion(new Egal(s,new Constante(5050))) //  � voir ........
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Somme", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compiler();
		}catch(Exception e){
			//e.printStackTrace();
			fail(" compilation de Inf, Affectation,Sequence,TantQue,Afficher,Assertion ??? " + e.getMessage());
		}
	}


	private static int somme(int n){
		int s=0; int i=0;
		while(i<n){
			i=i+1;
			s=s+i;
		}
		return s;
	}

	public void test_CompilationExecutionDeSomme_100() throws Exception{
		Contexte m = new Memoire();
		Variable n = new Variable(m,"n",100);
		Variable s = new Variable(m,"s",0);
		Variable i = new Variable(m,"i",0);

		Instruction inst =
				new Sequence(
						new Sequence(
								new TantQue(
										new Inf(i,n),
										new Sequence(
												new Affectation(i,new Addition(i,new Constante(1))),
												new Affectation(s,new Addition(s,i)))
										),
										new Afficher(s)),
										new Assertion(new Egal(s,new Constante(somme(100)))) //  � voir ........
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Somme2", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compilerEtExecuter();
		}catch(Exception e){
			//e.printStackTrace();
			fail(" ex�cution de " + inst.accepter(new VisiteurInstToString(ves,vbs)) + " ??? " +  e.getMessage());
		}
	}


	private static int fact(int n){
		if(n==0) return 1;
		else return n*fact(n-1);
	}

	public void test_CompilationExecutionDeFactoriel_5(){
		Contexte m = new Memoire();
		Variable x = new Variable(m,"x",5);
		Variable fact = new Variable(m,"fact",1);

		Instruction inst = 
				new Sequence(
						new TantQue(
								new Sup(x,new Constante(1)),
								new Sequence(
										new Affectation(fact,new Multiplication(fact,x)),
										new Affectation(x,new Soustraction(x,new Constante(1))))
								),
								new Assertion(new Egal(fact,new Constante(fact(5)))));

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Fact2", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compilerEtExecuter();
		}catch(Exception e){
			//e.printStackTrace();
			fail(m + inst.accepter(new VisiteurInstToString(ves,vbs))  + e.getMessage() + " ??? ");
		}
	}     


	private static int mult(int a, int b){
		int z = 0 ;
		while (b > 0){
			if ((b -(b / 2)*2) == 1){
				z = z + a  ; b = b-1;
			}else{
				a = 2 * a ; b = b / 2;
			}
		}
		return z;
	}

	public void test_CompilationExecutionDeMult_7_85() throws Exception{
		Contexte m = new Memoire();
		Variable a = new Variable(m,"a",7);
		Variable b = new Variable(m,"b",85);
		Variable z = new Variable(m,"z",0);

		Instruction inst = 
				new Sequence(
						new TantQue(
								new Sup(b,new Constante(0)),
								new Selection(
										new Egal(
												new Soustraction(b,new Multiplication(new Division(b,new Constante(2)),new Constante(2))),new Constante(1)),
												new Sequence(
														new Affectation(z,new Addition(z,a)),
														new Affectation(b,new Soustraction(b,new Constante(1))) 
														),
														new Sequence(
																new Affectation(a,new Multiplication(a,new Constante(2))),
																new Affectation(b,new Division(b,new Constante(2)))
																)
										)
								),
								new Assertion(new Egal(z,new Constante(mult(7,85))))
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Mult", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compilerEtExecuter();
		}catch(Exception e){
			fail(m + inst.accepter(new VisiteurInstToString(ves,vbs))  + e.getMessage() + " ??? ");
		}
	}

	public void test_CompilationExecutionDUneBouclePour() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new Sequence(
						new Pour( 
								new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), new Affectation(i,new Addition(i,new Constante(1))),
								new Affectation(j,new Addition(j,new Constante(1)))
								),
								new Assertion(new Egal(j,new Constante(6)))
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BouclePour", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compilerEtExecuter();
		}catch(Exception e){
			fail(m + inst.accepter(new VisiteurInstToString(ves,vbs))  + e.getMessage() + " ??? ");
		}
	}

	public void test_CompilationDeMult() throws Exception{
		Contexte m = new Memoire();
		Variable a = new Variable(m,"a",7);
		Variable b = new Variable(m,"b",85);
		Variable z = new Variable(m,"z",0);

		Instruction inst = 
				new Sequence(
						new TantQue(
								new Sup(b,new Constante(0)),
								new Selection(
										new Egal(
												new Soustraction(b,new Multiplication(new Division(b,new Constante(2)),new Constante(2))),new Constante(1)),
												new Sequence(
														new Affectation(z,new Addition(z,a)),
														new Affectation(b,new Soustraction(b,new Constante(1))) 
														),
														new Sequence(
																new Affectation(a,new Multiplication(a,new Constante(2))),
																new Affectation(b,new Division(b,new Constante(2)))
																)
										)
								),
								new Assertion(new Egal(z,new Constante(7*85)))
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_Mult", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Sup,Egal, Affectation,Sequence,Selection,TantQue,Assertion ??? " + e.getMessage());
		}
	}



	public void test_CompilationDUneBouclePour() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new Sequence(
						new Pour( 
								new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), new Affectation(i,new Addition(i,new Constante(1))),
								new Affectation(j,new Addition(j,new Constante(1)))
								),
								new Afficher(j)
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BouclePour", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Sequence,Pour,Afficher ??? " + e.getMessage());
		}
	}


	public void test_CompilationDUneBouclePourAvecAfficher() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new Sequence(
						new Pour( 
								new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
								new Sequence(
										new Affectation(i,new Addition(i,new Constante(1))),
										new Afficher(i)), 
										new Affectation(j,new Addition(j,new Constante(1)))
								),
								new Afficher(j)
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BouclePourEtAfficher", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Sequence,Pour,Afficher ??? " + e.getMessage());
		}
	}

	public void test_CompilationDeBouclesTantQueImbriquees() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new TantQue( 
						new Inf(i,new Constante(10)),
						new Sequence(
								new TantQue(
										new Inf(j,new Constante(10)),
										new Affectation(j,new Addition(j,new Constante(1)))
										),
										new Sequence(
												new Affectation(i,new Addition(i,new Constante(1))),
												new Affectation(j, new Constante(1))
												)
								)
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BoucleBoucle", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			System.err.println(inst.accepter(new VisiteurInstToString(ves,vbs)));
			fail("compilation de Inf, Affectation,Sequence,TantQue ??? " + e.getMessage());
		}
	}	

	public void test_CompilationDeBouclesPourImbriquees()
			throws Exception{
		question1.Contexte m = new Memoire();
		Variable x = new Variable(m, "x", Integer.valueOf(5));
		Variable k = new Variable(m, "k", Integer.valueOf(0));
		Variable n = new Variable(m, "n", Integer.valueOf(0));
		Variable f = new Variable(m, "f", Integer.valueOf(1));
		Variable g = new Variable(m, "g", Integer.valueOf(0));
		Variable fact = new Variable(m, "fact", Integer.valueOf(1));
		Instruction inst = new Pour(new Affectation(k, n), new Inf(k, x), new Pour(new Affectation(k, n), new Inf(k, x), new Pour(new Affectation(k, n), new Inf(k, x), new Affectation(k, new Addition(k, f)), new Affectation(k, new Addition(k, f))), new Affectation(k, new Addition(k, f))), new Affectation(k, new Addition(k, f)));
		@SuppressWarnings("rawtypes")
		question1.VisiteurExpression ves = new VisiteurInfixe(m);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		question2.VisiteurExpressionBooleenne vbs = new VisiteurBoolToJava(ves);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		VisiteurInstruction vs = new VisiteurInstToJava(ves, vbs, 4);
		@SuppressWarnings("unchecked")
		Z_ClasseJava cj = new Z_ClasseJava("X_PourClasseTest", "question3", inst, vs);
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Pour ??? " + e.getMessage());
		}
	}


	public void test_CompilationDeBouclesTantQuePourImbriquees() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new TantQue( 
						new Inf(i,new Constante(10)),
						new Sequence(
								new TantQue(
										new Inf(j,new Constante(10)),
										new Pour( 
												new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
												new Sequence(
														new Affectation(i,new Addition(i,new Constante(1))),
														new Afficher(i)), 
														new Affectation(j,new Addition(j,new Constante(1)))
												)),
												new Sequence(
														new Affectation(i,new Addition(i,new Constante(1))),
														new Affectation(j, new Constante(1))
														)
								)
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BoucleTantQuePour", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Sequence,TantQue,Pour ??? " + e.getMessage());
		}
	}	

	public void test_CompilationDeBouclesTantQuePourImbriquees_bis() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new TantQue( 
						new Inf(i,new Constante(10)),
						new Sequence(
								new TantQue(
										new Inf(j,new Constante(10)),
										new Pour( 
												new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
												new Sequence(
														new Affectation(i,new Addition(i,new Constante(1))),
														new Pour( 
																new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
																new Sequence(
																		new Affectation(i,new Addition(i,new Constante(1))),
																		new Afficher(i)), 
																		new Affectation(j,new Addition(j,new Constante(1)))
																)), 
																new Affectation(j,new Addition(j,new Constante(1)))
												)),
												new Sequence(
														new Affectation(i,new Addition(i,new Constante(1))),
														new Affectation(j, new Constante(1))
														)
								)
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BoucleTantQuePour2", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Sequence,TantQue,Pour ??? " + e.getMessage());
		}
	}	


	public void test_CompilationDeBouclesPourTantQueImbriquees_bis() throws Exception{
		Contexte m = new Memoire();
		Variable i = new Variable(m,"i");
		Variable j = new Variable(m,"j",1);

		Instruction inst = 
				new Pour( 
						new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
						new TantQue( 
								new Inf(i,new Constante(10)),
								new Sequence(
										new TantQue(
												new Inf(j,new Constante(10)),
												new Pour( 
														new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
														new Sequence(
																new Affectation(i,new Addition(i,new Constante(1))),
																new Pour( 
																		new Affectation(i,new Constante(0)), new Inf(i, new Constante(5)), 
																		new Sequence(
																				new Affectation(i,new Addition(i,new Constante(1))),
																				new Afficher(i)), 
																				new Affectation(j,new Addition(j,new Constante(1)))
																		)), 
																		new Affectation(j,new Addition(j,new Constante(1)))
														)),
														new Sequence(
																new Affectation(i,new Addition(i,new Constante(1))),
																new Affectation(j, new Constante(1))
																)
										)
								),
								new Affectation(j,new Addition(j,new Constante(1)))
						);

		VisiteurExpression<String> ves = new VisiteurInfixe(m);
		VisiteurExpressionBooleenne<String> vbs = new VisiteurBoolToJava(ves);
		VisiteurInstruction<String> vs = new VisiteurInstToJava(ves,vbs,4);

		Z_ClasseJava cj = new Z_ClasseJava("X_BoucleTantQuePour3", "question3", inst, vs);
		//System.out.println(cj.sourceComplet());
		//cj.enFichier();
		try{
			cj.compiler();
		}catch(Exception e){
			fail(" compilation de Inf, Affectation,Sequence,TantQue,Pour ??? " + e.getMessage());
		}
	}

	// 	public void test_CompilationDUneBoucleRepeter() throws Exception{
	// 	  Contexte m = new Memoire();
	// 	  Variable i = new Variable(m,"i");
	// 	  Variable j = new Variable(m,"j",1);
	// 
	// 	  Instruction inst = 
	// 	    new Sequence(
	//         new RepeterJusque( 
	//           new Sequence(
	//             new Affectation(i,new Addition(i,new Constante(1))),
	// 	          new Affectation(j,new Addition(j,new Constante(1)))
	// 	        ),
	// 	        new Ou(new Sup(i,new Constante(10)),new Egal(i,new Constante(10)))),
	//         new Afficher(j)
	//       );
	//      
	//     VisiteurExpression ves = new VisiteurInfixe(m);
	//     VisiteurExpressionBooleenne vbs = new VisiteurBoolToJava(ves);
	//     VisiteurInstruction vs = new VisiteurInstToJava(ves,vbs,4);
	//     
	//     Z_ClasseJava cj = new Z_ClasseJava("BoucleRepeter", "question3", inst, vs);
	//     //System.out.println(cj.sourceComplet());
	//     //cj.enFichier();
	//     try{
	//       cj.compiler();
	//     }catch(Exception e){
	//       fail(e.getMessage());
	//     }
	// 	}




}




